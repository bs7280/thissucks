
public class DatabaseFunctions {
	  //Database backend
	  DatabaseBackend db = new DatabaseBackend();
	
	  //Constructor
	  public DatabaseFunctions() {
		initialData();
	  }	
	  
	  public void initialData() {
		  db.insertTheaterPost("This is the title of a post", "This is the body of a post", 1);
		  db.insertTheaterPost("What a good theater", "I really liked this theater", 1);
		  db.insertTheaterPost("I did not have a good time", "This movie theater is awful!", 1);
		  db.insertTheaterPost("2/10 would not do again", "So I went to the movies to watch a movie, and I saw a movie, but going to the place to watch the movies was not as great as one would think going to the place to see the moives would be", 1);
	  
		  db.insertMoviePost("This movie rocked!", "I really liked this movie", 1);
		  db.insertMoviePost("Good movie", "yea so that movie was great", 1);
		  db.insertMoviePost("WOw that was good!", "Everyone should go check out this movie! it was super dope!", 1);
		  db.insertMoviePost("Potato", "I like potatoes", 1);
		  
		  db.addTheaterComment(0, 1, "this is the first comment");		  
		  db.addTheaterComment(0, 1, "another comment");		  
		  db.addTheaterComment(0, 1, "hey look a comment");		  
		  db.addTheaterComment(0, 1, "test test");		  
		  db.addTheaterComment(0, 1, "comment");

		  db.addMovieComment(0, 2, "first comment");	
		 // db.addMovieComment(2, 2, "asdfasdfasdf");	
		  //db.addMovieComment(2, 2, "tdsafasdf comment");	
		  //db.addMovieComment(1, 2, "pioujih");	
		  //db.addMovieComment(4, 2, "gjyyj");	
	  }
	
	  //This function gets a list of the top users based on credit points
	  //I dont know if there needs to be a limit to the number of users to return
	  public String[][] getTopUsers() {
	    String[][] returnValue = {
	      {"User1", "100 points"},
	      {"other guy", "86 points"},
	      {"someBro", "69 points"}, 
	      {"someOtherGuy", "45 points"},
	      {"notThatGreatUser", "22 points"}};
	
	    return returnValue;
	  }
	  
	//this function should take in the given parameters and add a post about given theater
	  public int create_theater_Post(int theaterId, String PostTitle, String Post_content, int User_id) {
	  	//this needs to add a post about a theater with the given parameters using theaterId as the id of the theater
		
		db.insertTheaterPost(PostTitle, Post_content, User_id);
		  
	  	return 1;
	
	  }
	
	
	  //this function should take in the given parameters and add a post about given movie
	  public int create_movie_Post(int movieId, String PostTitle, String Post_content, int User_id) {
	  	//this needs to add a post about a movie with the given parameters using movieId as the id of the movie
		  
		db.insertMoviePost(PostTitle, Post_content, User_id);
		  
	  	return 1;
	
	  }
	
	  //will connect to the database and return a list of title of posts about movies
	  public String[] getMoviePosts() {
	   /* String[] posts = {"The Notebook Sucked", "Ted 2 was hilarious and Tom Brady is awesome",
	                      "The Martian was very exciting",
	                      "some other movie title"};*/
	
	    return db.getAllMoviePostTitles();
	  }
	
	  //function that will connect to the database and returns a list of titles of posts about theaters
	  public String[] getTheatrePosts() {
	    //dummy data for getTheatrePosts
	    //String[] posts = {"Joes theater is garbage", "AMC rivernorth is pretty nice", "Amc needs to get better popcorn", "some other title!"};
	
	    return db.getAllTheaterPostTitles();
	  }
	  
	  public String[] getWorstTheaterPosts() {
	    //dummy data for getTheatrePosts
	    String[] posts = {"Joes theater is garbage", "AMC rivernorth is pretty nice", "Amc needs to get better popcorn", "some other title!"};
	
	    return posts;
	  }
	  
	  public String[] getWorstMoviePosts() {
	    //dummy data for getTheatrePosts
		    String[] posts = {"The Notebook Sucked", "Ted 2 was hilarious and Tom Brady is awesome",
                    "The Martian was very exciting",
                    "some other movie title"};
	
	    return posts;
	  }
	
	  //function that takes in an id, gets theater post with that id
	  public String[] getMoviePostByID(int postID) {
	      //String[] returnValue = {"This is a title of the post", "This is the body of the post. THe movie was pretty good. Or maby it wasn't. who knows."};
	
	      return db.getMoviePostById(postID);
	  }
	
	
	  //function that takes in an id, gets theater post with that id
	  public String[] getTheaterPostByID(int postID) {
	      //String[] returnValue = {"This is a title of the post", "This is the body of the post. The theater was not that nice, in fact, it sucked. and I am writing a movie review for a movie theater because I am a loser with no life. Like seriously. Who writes reviews for a movie theater. GO outside!"};

	      return db.getTheaterPostById(postID);
	  }
	
	  //Gets the comments from movie post given a post id
	  //NOTE! if the boolean  limit to three is true, then only get the top three comments
	  //from the database, ordered by upvotes
	  public CommentClass[] getMoviePostComments(int postID, boolean limitToThree) {
	
	      //setting the return value
	      CommentClass[] returnValue = new CommentClass[3];
	      
	      returnValue[0] = new CommentClass("someguy", 3, "the body of the comment", 8);
	      returnValue[1] = new CommentClass("someOtherGuy", 5, "this is a comment", 9);
	      returnValue[2] = new CommentClass("joe", 8, "my comment is fire", 12);
	
	      return db.getThreeCommonMovieComments(postID);
	  }
	
	
	  //Gets the comments from theater post given a post id
	  //NOTE! if the boolean  limit to three is true, then only get the top three comments
	  //from the database, ordered by upvotes
	  public CommentClass[] getTheaterPostComments(int postID, boolean limitToThree) {
	
	      //setting the return value
	      CommentClass[] returnValue = new CommentClass[3];
	      
	      returnValue[0] = new CommentClass("someguy", 3, "the body of the comment", 10);
	      returnValue[1] = new CommentClass("someOtherGuy", 5, "this is a comment", 5);
	      returnValue[2] = new CommentClass("joe", 8, "my comment is fire", 3);
	
	      return db.getThreeCommonTheaterComments(postID);
	  }
	
	  //Adds a comment for a theater post, takes in a commentId, userId, and body
	  public int addTheaterPostComment(int userId, String body, int postId) {
		  
		  db.addTheaterComment(postId, userId, body);
		  
		  return 1;
	  }
	
	  //Adds a comment for a Movie post, takes in a commentId, userId, and body
	  public int addMoviePostComment(int userId, String body, int postId) {

		  db.addMovieComment(postId, userId, body);
		  
	      return 1;
	  }
	
	  //Upvote a movie post
	  public int upvoteMoviePostComment(int commentId) {
		  
	      return 1;
	  }
	
	  //Upvote a theater post
	  public int upvoteTheaterPostComment(int commentId) {
	      return 1;
	  }

	  //Upvote a movie post
	  public void upvoteMoviePost(int postId) {
		  db.incrementMovieScore(postId);
	  }
	  
	  //downvote a movie post
	  public void downvoteMoviePost(int postId) {
		  db.decrementMovieScore(postId);
	  }

	  //Upvote a theater post
	  public void upvoteTheaterPost(int postId) {
		  System.out.println(postId);
		  db.incrementTheaterScore(postId);
	  }
	  
	  //downvote a theater post
	  public void downvoteTheaterPost(int postId) {
		  db.decrementTheaterScore(postId);
	  }
	  
	  public int getNumberOfTheaterPostVotes(int postId) {
		  return db.getTheaterPostScore(postId);
	  }
	  
	  public int getNumberOfMoviePostVotes(int postId) {
		  return db.getMoviePostScore(postId);
	  }
	  public String getTopCommentsUser() {
		  return "someGuy";
	  }
	  public int getTheaterCommentUpVotes(int commentId) {
		  return 1;
	  }
	  public int getMovieCommentUpVotes(int commentId) {
		  return 1;
	  }
	  
	  
	  // ----------------------- testing for database stuff ---------------------- //
	  
}
