import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.table.*;

import java.awt.BorderLayout;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.CardLayout;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import javax.swing.JScrollPane;

public class ForumUI {

	private JFrame frame;
	private static DatabaseFunctions dbInterface;
	JPanel startPanel;
	JPanel theaterPostPanel;
	JLabel theaterPostTitle;
	JLabel theaterPostBody;
	private JTable table;
	private JTextField commentText;
	private JButton submitCommentButton;
	
	//this variable keeps track of the postID
	private int theaterPostID = 0;
	private JButton addTheaterThreadButton;
	private JPanel addTheaterThread;
	private JLabel lblEnterTitle;
	private JLabel lblEnterBody;
	private JTextField txtTitle;
	private JTextField txtBody;
	private JButton btnSubmit;
	private JButton gotoMovieForumButton;
	private JLabel numberOfVotes;
	private JLabel theTopUser;
	private JButton viewWorstThreads;
	private JPanel worstTheaterThreads;
	private JTable worstThreadsTable;
	private JLabel lblWorstThreads;
	private JPanel moviesStart;
	private JTable moviesPostsTable;
	private JButton addMoviePostButton;
	private JButton seeWorstMoviePosts;
	private JPanel worstMovieThreads;
	private JTable worstMoviesTable;
	private JLabel lblWorstMovieThreads;
	private JPanel viewMovieThread;
	private JPanel addMovieThread;
	private JLabel movieEnterTitle;
	private JLabel movieEnterBody;
	private JTextField movieTitle;
	private JTextField movieBody;
	private JButton movieSubmit;

	JLabel moviePostTitle;
	JLabel moviePostBody;
	JLabel movienumberOfVotes;
	private JTextField moviecommentText;
	JButton moviesubmitCommentButton;
	private JButton btnBackToStart;
	private JButton btnBackToStart_1;
	private JButton movieCommentSubmit;
	
	
	
	
	
	

	// Launch the application.
	public static void main(String[] args) {
		//creating the interface for the database
		dbInterface = new DatabaseFunctions();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ForumUI window = new ForumUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//Create Application
	public ForumUI() {
		initialize();
	}


	// Initialize the contents of the frame.
	private void initialize() {
		//Initial Frame
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		//Creating the initial panel
		startPanel = new JPanel();
		startPanel.setBounds(0, 0, 607, 421);
		frame.getContentPane().add(startPanel);
		startPanel.setLayout(new CardLayout(0, 0));
		
		// --------------------  the starting point for the ui -------------------- //
		
		//the first panel
		JPanel panel = new JPanel();
		panel.setPreferredSize(new Dimension(300,300));
		startPanel.add(panel, "startUi");
		
		JButton gotoTheaterForumButtom = new JButton("goto theater forum");
		gotoTheaterForumButtom.setBounds(137, 5, 170, 25);
		gotoTheaterForumButtom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setCardVisibility("theaterStart");
			}
		});
		panel.setLayout(null);
		panel.add(gotoTheaterForumButtom);
		
		gotoMovieForumButton = new JButton("goto movie forum");
		gotoMovieForumButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setCardVisibility("moviesStart");
			}
		});
		gotoMovieForumButton.setBounds(147, 42, 157, 25);
		panel.add(gotoMovieForumButton);
		
		//Creating top user label
		theTopUser = new JLabel("user with most comments: " + (dbInterface.getTopCommentsUser()));
		theTopUser.setBounds(88, 91, 281, 15);
		panel.add(theTopUser);
		
		//  -------------------- Theater starting point -------------------- //
		
		//The second panel
		JPanel theaterStart = new JPanel();
		theaterStart.setPreferredSize(new Dimension(300,300));
		startPanel.add(theaterStart, "theaterStart");
		
		//Getting the titles of the Theater posts
		String[] theaterPosts = dbInterface.getTheatrePosts();
		//Getting the table contents
		TableModel dataModel = getTablePostsArray(theaterPosts, false);
		//Creating the table
		table = new JTable(dataModel);
		table.addMouseListener( new java.awt.event.MouseAdapter() {
			@Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = table.rowAtPoint(evt.getPoint());
		        int col = table.columnAtPoint(evt.getPoint());
		        if (row >= 0 && col >= 0) {
		        	int val = Integer.parseInt("" + table.getValueAt(row, 0));
		        	showTheaterPostPage(val);
		        }
		    }
		});
		theaterStart.add(table);
		
		addTheaterThreadButton = new JButton("Add Thread");
		addTheaterThreadButton.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
            	addTheaterThread();
            }
        });      
		theaterStart.add(addTheaterThreadButton);
		
		viewWorstThreads = new JButton("View Worst");
		viewWorstThreads.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCardVisibility("viewWorstThreads");
			}
		});
		theaterStart.add(viewWorstThreads);
		
		// -------------------- worst theater threads section  -------------------- //
		
		worstTheaterThreads = new JPanel();
		startPanel.add(worstTheaterThreads, "viewWorstThreads");
		
		lblWorstThreads = new JLabel("Worst Threads");
		worstTheaterThreads.add(lblWorstThreads);
		
		//Getting the table contents
		String[] worstTheaterPosts = dbInterface.getWorstTheaterPosts();
		TableModel worstTablesData = getTablePostsArray(worstTheaterPosts, true);
		//Creating the table
		worstThreadsTable = new JTable(worstTablesData);
		worstThreadsTable.addMouseListener( new java.awt.event.MouseAdapter() {
			@Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = worstThreadsTable.rowAtPoint(evt.getPoint());
		        int col = worstThreadsTable.columnAtPoint(evt.getPoint());
		        if (row >= 0 && col >= 0) {
		        	int val = Integer.parseInt("" + table.getValueAt(row, 0));
		        	showTheaterPostPage(val);
		        }
		    }
		});
		worstTheaterThreads.add(worstThreadsTable);
		
		//  --------------------  theater Post page section  -------------------- //
		
		theaterPostPanel = new JPanel();
		startPanel.add(theaterPostPanel, "theaterPost");
		theaterPostPanel.setLayout(null);
		
		theaterPostTitle = new JLabel("The Title");
		theaterPostTitle.setBounds(44, 48, 231, 15);
		theaterPostPanel.add(theaterPostTitle);
		
		theaterPostBody = new JLabel("The Body");
		theaterPostBody.setBounds(44, 39, 372, 143);
		theaterPostPanel.add(theaterPostBody);
		
		JButton theaterPostUpvote = new JButton("Upvote");
		theaterPostUpvote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Upvote the post
				dbInterface.upvoteTheaterPost(theaterPostID);
				numberOfVotes.setText("Votes: " + (dbInterface.getNumberOfTheaterPostVotes(theaterPostID) + 1));
			}
		});
		theaterPostUpvote.setBounds(212, 12, 117, 25);
		theaterPostPanel.add(theaterPostUpvote);
		
		JButton theaterPostDownvote = new JButton("Downvote");
		theaterPostDownvote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//downvote the post
				dbInterface.downvoteTheaterPost(theaterPostID);
				numberOfVotes.setText("Votes: " + (dbInterface.getNumberOfTheaterPostVotes(theaterPostID) - 1));
				
			}
		});
		theaterPostDownvote.setBounds(329, 7, 117, 25);
		theaterPostPanel.add(theaterPostDownvote);
		
		commentText = new JTextField();
		commentText.setText("Add a comment");
		commentText.setBounds(112, 252, 114, 19);
		theaterPostPanel.add(commentText);
		commentText.setColumns(10);
		
		submitCommentButton = new JButton("Submit comment");
		submitCommentButton.setBounds(237, 249, 179, 25);
		theaterPostPanel.add(submitCommentButton);
		submitCommentButton.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
                //Execute when button is pressed
                System.out.println(commentText.getText());
                dbInterface.addTheaterPostComment(getUserId(), commentText.getText(), theaterPostID);
            }
        });      
		
		JLabel lblAddComment = new JLabel("Add Comment");
		lblAddComment.setBounds(12, 254, 97, 15);
		theaterPostPanel.add(lblAddComment);
		
		if(getUserId() == -1) {
			lblAddComment.setVisible(false);
			commentText.setVisible(false);
			submitCommentButton.setVisible(false);
		}
		
		int numVotes = dbInterface.getNumberOfTheaterPostVotes(theaterPostID);
		numberOfVotes = new JLabel("Votes: " + numVotes);
		numberOfVotes.setBounds(346, 48, 70, 15);
		theaterPostPanel.add(numberOfVotes);
		
		//  --------------------  add theater thread section  -------------------- //
		
		addTheaterThread = new JPanel();
		startPanel.add(addTheaterThread, "addTheaterThread");
		addTheaterThread.setLayout(null);
		
		lblEnterTitle = new JLabel("Enter Title:");
		lblEnterTitle.setBounds(31, 29, 117, 15);
		addTheaterThread.add(lblEnterTitle);
		
		lblEnterBody = new JLabel("Enter Body");
		lblEnterBody.setBounds(32, 90, 96, 15);
		addTheaterThread.add(lblEnterBody);
		
		txtTitle = new JTextField();
		txtTitle.setText("Title");
		txtTitle.setBounds(171, 50, 114, 19);
		addTheaterThread.add(txtTitle);
		txtTitle.setColumns(10);
		
		txtBody = new JTextField();
		txtBody.setText("Body");
		txtBody.setBounds(171, 119, 114, 19);
		addTheaterThread.add(txtBody);
		txtBody.setColumns(10);
		
		btnSubmit = new JButton("Submit");
		btnSubmit.setBounds(301, 217, 117, 25);
		btnSubmit.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
            	dbInterface.create_theater_Post(1, txtTitle.getText(), txtBody.getText(), getUserId());
            	setCardVisibility("startUi");
            }
        });  
		addTheaterThread.add(btnSubmit);
		
		// -------------------- movies start -------------------- //
		
		moviesStart = new JPanel();
		startPanel.add(moviesStart, "moviesStart");
		
		
		//Getting the titles of the Theater posts
		String[] moviePosts = dbInterface.getMoviePosts();
		//Getting the table contents
		TableModel moviePostsDataModel = getMoviePostsArray(moviePosts, false);
		//Creating the table
		moviesPostsTable = new JTable(moviePostsDataModel);
		moviesPostsTable.setFillsViewportHeight(true);
		moviesPostsTable.addMouseListener( new java.awt.event.MouseAdapter() {
			@Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = moviesPostsTable.rowAtPoint(evt.getPoint());
		        int col = moviesPostsTable.columnAtPoint(evt.getPoint());
		        if (row >= 0 && col >= 0) {
		        	int val = Integer.parseInt("" + moviesPostsTable.getValueAt(row, 0));
		        	System.out.println(val);
		        	showMoviePostPage(val);
		        }
		    }
		});
		
		moviesStart.add(moviesPostsTable);
		
		addMoviePostButton = new JButton("Add Post");
		addMoviePostButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Goto add movies post thing
				setCardVisibility("addMovieThread");
			}
		});
		moviesStart.add(addMoviePostButton);
		
		seeWorstMoviePosts = new JButton("See worst posts");
		seeWorstMoviePosts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//goto worst movies thing
				setCardVisibility("worstMovieThreads");
			}
		});
		moviesStart.add(seeWorstMoviePosts);
		
		// ------------ worst movie threads ----------------- //
		
		worstMovieThreads = new JPanel();
		startPanel.add(worstMovieThreads, "worstMovieThreads");
		
		lblWorstMovieThreads = new JLabel("Worst Movie Threads");
		worstMovieThreads.add(lblWorstMovieThreads);
		
		
		//Getting the titles of the Theater posts
		String[] worstMoviePosts = dbInterface.getWorstMoviePosts();
		//Getting the table contents
		TableModel worstMoviePostsDataModel = getTablePostsArray(worstMoviePosts, true);
		//Creating the table
		worstMoviesTable = new JTable(worstMoviePostsDataModel);
		worstMoviesTable.addMouseListener( new java.awt.event.MouseAdapter() {
			@Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = worstMoviesTable.rowAtPoint(evt.getPoint());
		        int col = worstMoviesTable.columnAtPoint(evt.getPoint());
		        if (row >= 0 && col >= 0) {
		        	int val = Integer.parseInt("" + worstMoviesTable.getValueAt(row, 0));
		        	System.out.println(val);
		        	showMoviePostPage(val);
		        }
		    }
		});
		
		worstMovieThreads.add(worstMoviesTable);
		
		// --------------------- View movie thread ------------------- //
		
		
		viewMovieThread = new JPanel();
		startPanel.add(viewMovieThread, "viewMovieThread");
		viewMovieThread.setLayout(null);
		
		
		moviePostTitle = new JLabel("The Title");
		moviePostTitle.setBounds(23, 57, 277, 15);
		viewMovieThread.add(moviePostTitle);
		
		moviePostBody = new JLabel("The Body");
		moviePostBody.setBounds(46, 57, 296, 114);
		viewMovieThread.add(moviePostBody);
		
		JButton moviePostUpvote = new JButton("Upvote");
		moviePostUpvote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Upvote the post
				dbInterface.upvoteMoviePost(theaterPostID);
				movienumberOfVotes.setText("Votes: " + (dbInterface.getNumberOfMoviePostVotes(theaterPostID) + 1));
			}
		});
		moviePostUpvote.setBounds(164, 5, 84, 25);
		viewMovieThread.add(moviePostUpvote);
		
		JButton moviePostDownvote = new JButton("Downvote");
		moviePostDownvote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//downvote the post
				dbInterface.downvoteMoviePost(theaterPostID);
				movienumberOfVotes.setText("Votes: " + (dbInterface.getNumberOfMoviePostVotes(theaterPostID) - 1));
				
			}
		});
		moviePostDownvote.setBounds(253, 5, 105, 25);
		viewMovieThread.add(moviePostDownvote);
		
		moviecommentText = new JTextField();
		moviecommentText.setText("Add a comment");
		moviecommentText.setBounds(24, 233, 114, 19);
		viewMovieThread.add(moviecommentText);
		moviecommentText.setColumns(10);
		
		moviesubmitCommentButton = new JButton("Submit comment");
		moviesubmitCommentButton.setBounds(237, 249, 179, 25);
		theaterPostPanel.add(moviesubmitCommentButton);
		
		btnBackToStart = new JButton("Back to start");
		btnBackToStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCardVisibility("startUi");
			}
		});
		btnBackToStart.setBounds(269, 212, 147, 25);
		theaterPostPanel.add(btnBackToStart);
		moviesubmitCommentButton.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
                //Execute when button is pressed
                System.out.println(moviecommentText.getText());
                dbInterface.addMoviePostComment(getUserId(), moviecommentText.getText(), theaterPostID);
            }
        });
		
		final JLabel movieAddComment = new JLabel("Add Comment");
		movieAddComment.setBounds(482, 10, 97, 15);
		viewMovieThread.add(movieAddComment);
		
		if(getUserId() == -1) {
			movieAddComment.setVisible(false);
			moviecommentText.setVisible(false);
			moviesubmitCommentButton.setVisible(false);
		}
		
		numVotes = dbInterface.getNumberOfTheaterPostVotes(theaterPostID);
		movienumberOfVotes = new JLabel("Votes: " + numVotes);
		movienumberOfVotes.setBounds(274, 35, 58, 15);
		viewMovieThread.add(movienumberOfVotes);
		
		btnBackToStart_1 = new JButton("Back to start");
		btnBackToStart_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setCardVisibility("startUi");
			}
		});
		btnBackToStart_1.setBounds(316, 230, 117, 25);
		viewMovieThread.add(btnBackToStart_1);
		
		movieCommentSubmit = new JButton("Submit");
		movieCommentSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dbInterface.addMoviePostComment(getUserId(), movieAddComment.getText(), theaterPostID);
            	setCardVisibility("startUi");
			}
		});
		movieCommentSubmit.setBounds(152, 230, 117, 25);
		viewMovieThread.add(movieCommentSubmit);
		
		
		
		
		
		
		

		// --------------------- add movie thread ------------------- //
		
		
		
		addMovieThread = new JPanel();
		startPanel.add(addMovieThread, "addMovieThread");
		addTheaterThread.setLayout(null);
		
		movieEnterTitle = new JLabel("Enter Title:");
		movieEnterTitle.setBounds(31, 29, 117, 15);
		addMovieThread.add(movieEnterTitle);
		
		movieEnterBody = new JLabel("Enter Body");
		movieEnterBody.setBounds(32, 90, 96, 15);
		addMovieThread.add(movieEnterBody);
		
		movieTitle = new JTextField();
		movieTitle.setText("Title");
		movieTitle.setBounds(171, 50, 114, 19);
		addMovieThread.add(movieTitle);
		movieTitle.setColumns(10);
		
		movieBody = new JTextField();
		movieBody.setText("Body");
		movieBody.setBounds(171, 119, 114, 19);
		addMovieThread.add(movieBody);
		movieBody.setColumns(10);
		
		movieSubmit = new JButton("Submit");
		movieSubmit.setBounds(301, 217, 117, 25);
		movieSubmit.addActionListener(new ActionListener() {
 
            public void actionPerformed(ActionEvent e)
            {
            	dbInterface.create_movie_Post(1, movieTitle.getText(), movieBody.getText(), getUserId());
            	setCardVisibility("startUi");
            }
        });  
		addMovieThread.add(movieSubmit);
		
		
		

		setCardVisibility("bar");
	}
	
	//Function that allows me the program to specify which card to show
	private void setCardVisibility(String cardName) {
		
		if(cardName == "moviesStart") {

			moviesStart.remove(moviesPostsTable);

			//Getting the titles of the Theater posts
			String[] moviePosts = dbInterface.getMoviePosts();
			//Getting the table contents
			TableModel moviePostsDataModel = getMoviePostsArray(moviePosts, false);
			//Creating the table
			moviesPostsTable = new JTable(moviePostsDataModel);

			moviesPostsTable.setFillsViewportHeight(true);
			moviesPostsTable.addMouseListener( new java.awt.event.MouseAdapter() {
				@Override
			    public void mouseClicked(java.awt.event.MouseEvent evt) {
			        int row = moviesPostsTable.rowAtPoint(evt.getPoint());
			        int col = moviesPostsTable.columnAtPoint(evt.getPoint());
			        if (row >= 0 && col >= 0) {
			        	int val = Integer.parseInt("" + moviesPostsTable.getValueAt(row, 0));
			        	System.out.println(val);
			        	showMoviePostPage(val);
			        }
			    }
			});

			moviesStart.add(moviesPostsTable);
			
			System.out.println("potato");
		} else if (cardName == "viewWorstThreads") {
			
			

			worstTheaterThreads.remove(worstThreadsTable);
			//Getting the table contents
			String[] worstTheaterPosts = dbInterface.getWorstMoviePosts();
			TableModel worstTablesData = getMoviePostsArray(worstTheaterPosts, true);
			//Creating the table
			worstThreadsTable = new JTable(worstTablesData);
			worstThreadsTable.addMouseListener( new java.awt.event.MouseAdapter() {
				@Override
			    public void mouseClicked(java.awt.event.MouseEvent evt) {
			        int row = worstThreadsTable.rowAtPoint(evt.getPoint());
			        int col = worstThreadsTable.columnAtPoint(evt.getPoint());
			        if (row >= 0 && col >= 0) {
			        	int val = Integer.parseInt("" + table.getValueAt(row, 0));
			        	showMoviePostPage(val);
			        }
			    }
			});
			worstTheaterThreads.add(worstThreadsTable);
			
			
		}
		
		CardLayout cl = (CardLayout)(startPanel.getLayout());
	    cl.show(startPanel, cardName);
	}
	
	//Sets view to theater post page
	public void showTheaterPostPage(int postId) {
		//Having the UI make the theater post page be visible
		setCardVisibility("theaterPost");
		
		//Getting the title and body of the post
		String[] post = dbInterface.getTheaterPostByID(postId);
		
		
		//Updating the content of the post
		theaterPostTitle.setText(post[0]);
		theaterPostBody.setText(post[1]);
		
		//Setting the theater post id
		theaterPostID = postId;
	}
	
	//Sets view to theater post page
	public void showMoviePostPage(int postId) {
		//Having the UI make the theater post page be visible
		setCardVisibility("viewMovieThread");
		
		//Getting the title and body of the post
		String[] post = dbInterface.getMoviePostByID(postId);
		
		
		//Updating the content of the post
		moviePostTitle.setText(post[0]);
		moviePostBody.setText(post[1]);
		
		//Setting the theater post id
		theaterPostID = postId;
	}
	
	public void addTheaterThread() {
		setCardVisibility("addTheaterThread");
	}
	
	//TODO make this actually interact with the database
	private int getUserId() {
		return 1;
	}
	
	//This Generates the data in a posts array
	private TableModel getTablePostsArray(final String[] postTitles, boolean showWorst) {
		
		//Creating an array of what to actually display
		final ArrayList<String[]> tableContents = new ArrayList();
		
		if(showWorst) { //this creates the data for the worst threads table
			for (int i = 0; i < postTitles.length; i++) {
				//note that i should be the id
				//TODO make this actually deal with postID
				String[] things = {("" + i), postTitles[1], ("" + dbInterface.getNumberOfTheaterPostVotes(i))};  
	
				//adding the entry for the post
				tableContents.add(things);
			}
		} else { //best threads table
			for (int i = 0; i < postTitles.length; i++) {
				//note that i should be the id
				//TODO make this actually deal with postID
				String[] things = {("" + i), postTitles[0], ("" + dbInterface.getNumberOfTheaterPostVotes(i))};  
	
				//adding the entry for the post
				tableContents.add(things);
				
				//Adding the comments
				CommentClass[] thing2 = dbInterface.getTheaterPostComments(i, true);
				for(int n = 0; n < thing2.length; n++) {
					String[] entry = {("" + i), thing2[n].getBody(), ("" + thing2[n].getCommentUpVotes())};
					
					tableContents.add(entry);
				}
				
			}
		}
		
		//DataModel for table
	    TableModel dataModel = new AbstractTableModel() {
	        public int getColumnCount() { return 3; }
	        
	        public int getRowCount() { return (tableContents.size());}
	        
	        //Function, take in row and col location
	        public Object getValueAt(int row, int col) { 
	        	if(row >= tableContents.size()) {
	        		return new String("Null");
	        	} else {
	        		if(col == 0) { //id
	        			//TODO replace this with the id of the post
	        			return tableContents.get(row)[0];
	        		} else if(col == 1) { //title/comment body
	        			return tableContents.get(row)[1];
	        		} else {
	        			return tableContents.get(row)[2];
	        		}
	        	}
	        }
	        
	        
	    };
	    
	    return dataModel;
	}
	
	
	
	
	

	
	//This Generates the data in a posts array
	private TableModel getMoviePostsArray(final String[] postTitles, boolean showWorst) {
		
		//Creating an array of what to actually display
		final ArrayList<String[]> tableContents = new ArrayList();
		
		if(showWorst) { //this creates the data for the worst threads table
			for (int i = 0; i < postTitles.length; i++) {
				//note that i should be the id
				//TODO make this actually deal with postID
				String[] things = {("" + i), postTitles[i], ("" + dbInterface.getNumberOfMoviePostVotes(i))};  
	
				//adding the entry for the post
				tableContents.add(things);
			}
		} else { //best threads table
			for (int i = 0; i < postTitles.length; i++) {
				//note that i should be the id
				//TODO make this actually deal with postID
				String[] things = {("" + i), postTitles[i], ("" + dbInterface.getNumberOfMoviePostVotes(i))};  
	
				//adding the entry for the post
				tableContents.add(things);
				
				//Adding the comments
				CommentClass[] thing2 = dbInterface.getMoviePostComments(i, true);
				for(int n = 0; n < thing2.length; n++) {
					String[] entry = {("" + i), thing2[n].getBody(), ("" + thing2[n].getCommentUpVotes())};
					
					tableContents.add(entry);
				}
				
			}
		}
		
		//DataModel for table
	    TableModel dataModel = new AbstractTableModel() {
	        public int getColumnCount() { return 3; }
	        
	        public int getRowCount() { return (tableContents.size());}
	        
	        //Function, take in row and col location
	        public Object getValueAt(int row, int col) { 
	        	if(row >= tableContents.size()) {
	        		return new String("Null");
	        	} else {
	        		if(col == 0) { //id
	        			//TODO replace this with the id of the post
	        			return tableContents.get(row)[0];
	        		} else if(col == 1) { //title/comment body
	        			return tableContents.get(row)[1];
	        		} else {
	        			return tableContents.get(row)[2];
	        		}
	        	}
	        }
	        
	        
	    };
	    
	    return dataModel;
	}
	
	
	
	
	
}
