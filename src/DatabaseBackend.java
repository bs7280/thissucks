import java.util.ArrayList;
import java.util.Collections;

public class DatabaseBackend {
	//Arraylists for the posts
	
	//Theater posts: note,these array lists have an array of strings at each "row"
	//with each of these arrays having 4 items (in order): 
	//the title of the post, then the body of the post, then the number of votes, the userId
	//the same applies to movie posts
	ArrayList<String[]> theaterPosts = new ArrayList();
	ArrayList<String[]> moviePosts = new ArrayList();
	
	// -------------------- Arraylists for comments ---------------------- //
	
	//for comments there are 4 items (in order)
	//content, userId, theaterId, votes
	ArrayList<String[]> theaterComments = new ArrayList();
	ArrayList<String[]> movieComments = new ArrayList();
 	
	public DatabaseBackend() {
		
	}
	
	/////////////////////////////////////////////////////////
	//                                                     //
	//            Theater and Movie Post Stuff             //
	//                                                     //
	/////////////////////////////////////////////////////////
	
	// ------------- Main functionality abstractions -------------- //
	
	//Function to add a theater post
	public void insertTheaterPost(String title, String body, int userId) {
		//This is the fake row that will be inserted
		String[] insertRow = {title, body, "" + 0};
		
		//Adding the row to the arraylist
		theaterPosts.add(insertRow);
	}
	
	public void incrementTheaterScore(int loc) {
		setTheaterScore(loc, (getTheaterPostScore(loc) + 1));
	}
	
	public void decrementTheaterScore(int loc) {
		setTheaterScore(loc, (getTheaterPostScore(loc) - 1));
	}
	
		// ----------------- (Movies) -------------- //
	
	public void insertMoviePost(String title, String body, int userId) {
		//This is the fake row that will be inserted
		String[] insertRow = {title, body, "" + 0};
		
		//Adding the row to the arraylist
		moviePosts.add(insertRow);
	}
	
	public void incrementMovieScore(int loc) {
		setMovieScore(loc, (getMoviePostScore(loc) + 1));
		System.out.println(loc);
	}

	public void decrementMovieScore(int loc) {
		setMovieScore(loc, (getMoviePostScore(loc) - 1));
	}
	
	
	// ---------------------- Getters -------------------------- //

	//gets the score of a theater post
	public int getTheaterPostScore(int theaterPostId) {
		if(theaterPostId < theaterPosts.size()) {
			return Integer.parseInt("" + (theaterPosts.get(theaterPostId)[2]));
		} else {
			System.out.println("asdasdas");
			return 0;
		}
	}
	
	//gets the title of a movie post
	public String getTheaterPostTitle(int theaterPostId) {
		if(theaterPostId < theaterPosts.size()) {
			return theaterPosts.get(theaterPostId)[0];
		} else {
			return "Null";
		}
	}
	
	//gets the body of a theater post
	public String getTheaterPostBody(int theaterPostId) {
		if(theaterPostId < theaterPosts.size()) {
			return theaterPosts.get(theaterPostId)[1];
		} else {
			return "Null";
		}
	}

	//Returns the id of the most recently inserted theaterPost
	public int getLastTheaterPostId() {
		return  theaterPosts.size() - 1;
	}

	//Gets all the titles for a theater post
	public String[] getAllTheaterPostTitles() {
		//commentsTracker
		ArrayList<String> postsTracker = new ArrayList();
		
		for(int i = 0; i < theaterPosts.size(); i++) {
			postsTracker.add(theaterPosts.get(i)[0]);
		}
		
		String[] retArr = new String[postsTracker.size()];
		retArr = postsTracker.toArray(retArr);
		
		return retArr;
	}
	
	//Gets all the titles for a theater post
	public String[] getAllMoviePostTitles() {
		//commentsTracker
		ArrayList<String> postsTracker = new ArrayList();
		
		for(int i = 0; i < moviePosts.size(); i++) {
			postsTracker.add(moviePosts.get(i)[0]);
		}
		
		String[] retArr = new String[postsTracker.size()];
		retArr = postsTracker.toArray(retArr);
		
		return retArr;
	}

	//Gets all the titles for a theater post
	public String[] getTheaterPostById(int id) {
		if(id < theaterPosts.size()) {
			String[] retVal = {theaterPosts.get(id)[0],theaterPosts.get(id)[1]};
			return retVal;
		} else {
			String [] retVal = {"NULL", "NULL"};
			return retVal;
		}
	}

		// ------------ movies --------------- //

	//Gets all the titles for a theater post
	public String[] getMoviePostById(int id) {
		if(id < moviePosts.size()) {
			String[] retVal = {moviePosts.get(id)[0],moviePosts.get(id)[1]};
			return retVal;
		} else {
			String [] retVal = {"NULL", "NULL"};
			return retVal;
		}
	}
	
	//gets the score of a movie post
	public int getMoviePostScore(int theaterPostId) {
		if(theaterPostId < moviePosts.size()) {
			return Integer.parseInt("" + (moviePosts.get(theaterPostId)[2]));
		} else {
			return 0;
		}
	}
	
	//gets the title of a movie post
	public String getMoviePostTitle(int theaterPostId) {
		if(theaterPostId < moviePosts.size()) {
			return moviePosts.get(theaterPostId)[0];
		} else {
			return "Null";
		}
	}
	
	//gets the body of a movie post
	public String getMoviePostBody(int theaterPostId) {
		if(theaterPostId < moviePosts.size()) {
			return moviePosts.get(theaterPostId)[1];
		} else {
			return "Null";
		}
	}

	//Returns the id of the most recently inserted theaterPost
	public int getLastMoviePostId() {
		return  moviePosts.size() - 1;
	}
	
	//------------------------ SETTERS -------------------------- //

	//Method to set the title for a specific theaterPost
	private void setTheaterTitle(int loc, String title) {
		String[] newRow = {title, getTheaterPostBody(loc), ("" + getTheaterPostScore(loc))};
		theaterPosts.set(loc, newRow);
	}

	//method to set the body for a specific theaterPost
	private void setTheaterBody(int loc, String body) {
		String[] newRow = {getTheaterPostTitle(loc), body, ("" + getTheaterPostScore(loc))};
		theaterPosts.set(loc, newRow);
	}

	//method to set the score for a specific theaterPost
	private void setTheaterScore(int loc, int newScore) {
		String[] newRow = {getTheaterPostTitle(loc), getTheaterPostBody(loc), ("" + newScore)};
		theaterPosts.set(loc, newRow);
	}
	
		// -------------- Movies ---------------- //
	

	//Method to set the title for a specific theaterPost
	private void setMovieTitle(int loc, String title) {
		String[] newRow = {title, getMoviePostBody(loc), ("" + getMoviePostScore(loc))};
		moviePosts.set(loc, newRow);
	}

	//method to set the body for a specific theaterPost
	private void setMovieBody(int loc, String body) {
		String[] newRow = {getMoviePostTitle(loc), body, ("" + getMoviePostScore(loc))};
		moviePosts.set(loc, newRow);
	}

	//method to set the score for a specific theaterPost
	private void setMovieScore(int loc, int newScore) {
		String[] newRow = {getMoviePostTitle(loc), getMoviePostBody(loc), ("" + newScore)};
		moviePosts.set(loc, newRow);
	}



	/////////////////////////////////////////////////////////
	//                                                     //
	//            Theater/Movie Comment Stuff              //
	//                                                     //
	/////////////////////////////////////////////////////////

	//Take in theater post id, user id, and the content of the comment to create a theater comment
	public void addTheaterComment(int theaterPostId, int userId, String content) {
		String[] row = {content, ("" + userId), ("" + theaterPostId), "0"};
		
		theaterComments.add(row);
	}

	//Take in movie post id, user id, and the content of the comment to create a movie comment
	public void addMovieComment(int moviePostId, int userId, String content) {
		String[] row = {content, ("" + userId), ("" + moviePostId), "0"};
		
		movieComments.add(row);
	}
	
	
	// ------------------ getters ---------------- //

	private ArrayList<CommentClass> theaterCommentHelper(int postId) {
		ArrayList<CommentClass> commentsTracker = new ArrayList();
		
		for(int i = 0; i < theaterComments.size(); i++) {
			int rowTheaterPostId = Integer.parseInt(theaterComments.get(i)[2]);
			if(rowTheaterPostId == postId) {
				//add this to list
				
				CommentClass tmp = new CommentClass("Username", i, theaterComments.get(i)[0], i);
				commentsTracker.add(tmp);
			}
		}
		System.out.println(theaterComments.size());
		return commentsTracker;
	}

	private ArrayList<CommentClass> movieCommentHelper(int postId) {
		ArrayList<CommentClass> commentsTracker = new ArrayList();
		
		for(int i = 0; i < movieComments.size(); i++) {
			int rowTheaterPostId = Integer.parseInt(movieComments.get(i)[2]);
			if(rowTheaterPostId == postId) {
				//add this to list
				
				CommentClass tmp = new CommentClass("Username", i, movieComments.get(i)[0], i);
				commentsTracker.add(tmp);
			}
		}
		
		return commentsTracker;
	}
	
	//Gets all the comments for a theater post
	public CommentClass[] getAllCommentsForTheaterPost(int theaterPostId) {
		//commentsTracker
		ArrayList<CommentClass> commentsTracker = theaterCommentHelper(theaterPostId);
		
		CommentClass[] retArr = new CommentClass[commentsTracker.size()];
		retArr = commentsTracker.toArray(retArr);
		
		return retArr;
	}
	
	//gets all the comments for a movie post
	public CommentClass[] getAllCommentsForMoviePost(int theaterPostId) {
		//commentsTracker
		ArrayList<CommentClass> commentsTracker = movieCommentHelper(theaterPostId);
		
		CommentClass[] retArr = new CommentClass[commentsTracker.size()];
		retArr = commentsTracker.toArray(retArr);
		
		return retArr;
	}

	public CommentClass[] getThreeCommonTheaterComments(int postId) {
		ArrayList<CommentClass> arr = theaterCommentHelper(postId);
		
		Collections.sort(arr);
		
		if(arr.size() >= 3) {
			arr.subList(0, 2);	
		}
		
		
		CommentClass[] retArr = new CommentClass[arr.size()];
		retArr = arr.toArray(retArr);
		
		return retArr;
	}
	
	public CommentClass[] getThreeCommonMovieComments(int postId) {
		ArrayList<CommentClass> arr = movieCommentHelper(postId);
		
		Collections.sort(arr);
		
		if(arr.size() >= 3) {
			arr.subList(0, 2);	
		}
		
		
		CommentClass[] retArr = new CommentClass[arr.size()];
		retArr = arr.toArray(retArr);
		
		return retArr;
	}
	

}
