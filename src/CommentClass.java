import java.util.*;

public class CommentClass implements Comparable{ 
  private String username;
  private int commentId;
  private String commentBody;
  private int commentUpVotes;

  public CommentClass(String uname, int id, String body, int upvotes) {
    username = uname;
    commentId = id;
    commentBody = body;
    commentUpVotes = upvotes;
  }

  public String getUsername() {
    return username;
  }

  public int getCommentId() {
    return commentId;
  }

  public String getBody() {
    return commentBody;
  }

  public int getCommentUpVotes() {
    return commentUpVotes;
  }
  
  public int compareTo(CommentClass comparestu) {
      int compareage=((CommentClass)comparestu).commentUpVotes;
      /* For Ascending order*/
      return this.commentUpVotes-compareage;

      /* For Descending order do like this */
      //return compareage-this.studentage;
  }

@Override
public int compareTo(Object arg0) {
	// TODO Auto-generated method stub
	return 0;
}


}
